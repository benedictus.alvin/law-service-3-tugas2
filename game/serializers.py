from rest_framework import *
from rest_framework.serializers import *
from rest_framework.authtoken.models import *
from django.contrib.auth.models import *
from .models import *

class GameSerializer(ModelSerializer):

	class Meta:
		model = Game
		fields = "__all__"
