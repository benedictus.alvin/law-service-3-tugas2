from django.db import models
from django.contrib.auth.models import *

class Game(models.Model):
    title = models.CharField(max_length=30)
    description = models.CharField(max_length=200)
    genre = models.CharField(max_length=10)
    price = models.IntegerField(default =0)

    class Meta:
        db_table = 'Game'
